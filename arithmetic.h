#ifndef TYPING_ARITHMETIC_H
#define TYPING_ARITHMETIC_H

#include "utils.h"

namespace typing::arithmetic {

template <class Alias>
struct Add {};

template <class Alias>
auto operator+(const Add<Alias>& a, const Add<Alias>& b)
{
    return Alias(op_get(a) + op_get(b));
}

template <class Alias>
struct Sub {};

template <class Alias>
auto operator-(const Sub<Alias>& a, const Sub<Alias>& b)
{
    return Alias(op_get(a) - op_get(b));
}

template <class Alias>
struct Div {};

template <class Alias>
auto operator/(const Div<Alias>& a, const Div<Alias>& b)
{
    return Alias(op_get(a) / op_get(b));
}

template <class Alias>
struct Mul {};

template <class Alias>
auto operator*(const Mul<Alias>& a, const Mul<Alias>& b)
{
    return Alias(op_get(a) * op_get(b));
}

}

namespace typing {

template <typename Self>
struct Arithmetic:
    typing::arithmetic::Add<Self>,
    typing::arithmetic::Sub<Self>,
    typing::arithmetic::Div<Self>,
    typing::arithmetic::Mul<Self>
{};

}

#endif
