#ifndef TYPING_BITWISE_H
#define TYPING_BITWISE_H

#include "utils.h"

namespace typing::bitwise {

template <class Alias>
struct BitOr {};

template <class Alias>
auto operator|(const BitOr<Alias>& a, const BitOr<Alias>& b)
{
    return Alias(op_get(a) | op_get(b));
}

template <class Alias>
struct BitAnd {};

template <class Alias>
auto operator&(const BitAnd<Alias>& a, const BitAnd<Alias>& b)
{
    return Alias(op_get(a) & op_get(b));
}

template <class Alias>
struct Xor {};

template <class Alias>
auto operator^(const Xor<Alias>& a, const Xor<Alias>& b)
{
    return Alias(op_get(a) ^ op_get(b));
}

template <class Alias>
struct LShift {};

template <class Alias>
auto operator<<(const LShift<Alias>& a, const LShift<Alias>& b)
{
    return Alias(op_get(a) << op_get(b));
}

template <class Alias>
struct RShift {};

template <class Alias>
auto operator>>(const RShift<Alias>& a, const RShift<Alias>& b)
{
    return Alias(op_get(a) >> op_get(b));
}

}

namespace typing {

template <typename Self>
struct Bitwise:
    typing::bitwise::BitOr<Self>,
    typing::bitwise::BitAnd<Self>,
    typing::bitwise::Xor<Self>,
    typing::bitwise::LShift<Self>,
    typing::bitwise::RShift<Self>
{};

}

#endif
