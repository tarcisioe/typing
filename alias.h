#ifndef TYPING_ALIAS_H
#define TYPING_ALIAS_H

#include <utility>

#include "utils.h"

namespace typing {

template <typename T, typename Tag=void>
class Alias {
public:
    constexpr Alias():
        value{}
    {}

    explicit constexpr Alias(const T& value):
        value{value}
    {}

    explicit constexpr Alias(T&& value) noexcept(
            std::is_nothrow_constructible_v<T>
    ):
        value{std::move(value)}
    {}

    explicit operator T&() noexcept {
        return value;
    }

    explicit operator const T&() const noexcept {
        return value;
    }

    explicit operator T&&() noexcept {
        return std::move(value);
    }

    explicit operator const T&&() const noexcept {
        return std::move(value);
    }

    friend void swap(Alias& lhs, Alias& rhs) noexcept
    {
        using std::swap;
        swap(static_cast<T&>(lhs), static_cast<T&>(rhs));
    }

private:
    T value;
};

template <typename T, typename Tag>
decltype(auto) get(Alias<T, Tag>& a) noexcept
{
    return static_cast<T&>(a);
}

template <typename T, typename Tag>
decltype(auto) get(const Alias<T, Tag>& a) noexcept
{
    return static_cast<const T&>(a);
}

template <typename T, typename Tag>
decltype(auto) get(Alias<T, Tag>&& a) noexcept
{
    return static_cast<T&&>(a);
}

template <typename T, typename Tag>
decltype(auto) get(const Alias<T, Tag>&& a) noexcept
{
    return static_cast<const T&&>(a);
}

}

#endif
