#ifndef TYPING_IO_H
#define TYPING_IO_H

#include "utils.h"

namespace typing::io {

template <class Alias>
struct Show {};

template <class Stream, class Alias>
auto& operator<<(Stream& o, const Show<Alias>& s) {
    return o << op_get(s);
}

template <class Alias>
struct Read {};

template <class Stream, class Alias>
auto& operator>>(Stream& o, const Read<Alias>& s) {
    return o >> op_get(s);
}

}

#endif
