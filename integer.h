#ifndef TYPING_INTEGER_H
#define TYPING_INTEGER_H

#include <cstdint>
#include <type_traits>

#include "alias.h"
#include "arithmetic.h"
#include "bitwise.h"
#include "compare.h"
#include "io.h"

namespace typing {

template <typename T>
constexpr auto is_integer_v =
    std::is_integral_v<T> &&
    !std::is_same_v<T, bool> &&
    !std::is_same_v<T, char>;

template <typename Underlying, typename Self>
struct Integer: Alias<Underlying>,
                Arithmetic<Self>,
                Bitwise<Self>,
                Cmp<Self>,
                io::Read<Self>,
                io::Show<Self>
{
    static_assert(is_integer_v<Underlying>,
                  "Underlying type must be a true integer type.");
    using typing::Alias<Underlying>::Alias;
};

struct Int8: Integer<int8_t, Int8> {
    using Integer::Integer;
};

struct Int16: Integer<int16_t, Int16> {
    using Integer::Integer;
};

struct Int32: Integer<int32_t, Int32> {
    using Integer::Integer;
};

struct Int64: Integer<int64_t, Int64> {
    using Integer::Integer;
};

struct Uint8: Integer<uint8_t, Uint8> {
    using Integer::Integer;
};

struct Uint16: Integer<uint16_t, Uint16> {
    using Integer::Integer;
};

struct Uint32: Integer<uint32_t, Uint32> {
    using Integer::Integer;
};

struct Uint64: Integer<int64_t, Uint64> {
    using Integer::Integer;
};

}

#endif
