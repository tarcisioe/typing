#include <iostream>
#include <iomanip>

#include "integer.h"

int main()
{
    using namespace typing;

    auto i = Int32{42};
    auto j = Int32{54};

    auto u = Int64{1024};
    auto v = Int64{2048};

    std::cout << (i == j) << '\n';
    std::cout << (i == i) << '\n';

    std::cout << (i + j) << '\n';
    std::cout << (i - j) << '\n';
    std::cout << (i / j) << '\n';
    std::cout << (i * j) << '\n';

    auto b = Uint32{0xAAAAAAAA};
    auto c = Uint32{0xFFFFFFFF};

    std::cout << std::hex << (b ^ c) << std::dec << '\n';
}
