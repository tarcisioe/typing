#ifndef TYPING_COMPARE_H
#define TYPING_COMPARE_H

#include "utils.h"

namespace typing::compare {

template <class Alias>
struct Eq {};

template <class Alias>
auto operator==(const Eq<Alias>& a, const Eq<Alias>& b)
{
    return op_get(a) == op_get(b);
}

template <class Alias>
struct Neq {};

template <class Alias>
auto operator!=(const Neq<Alias>& a, const Neq<Alias>& b)
{
    return op_get(a) != op_get(b);
}

template <class Alias>
struct Lt {};

template <class Alias>
auto operator<(const Lt<Alias>& a, const Lt<Alias>& b)
{
    return op_get(a) < op_get(b);
}

template <class Alias>
struct Gt {};

template <class Alias>
auto operator>(const Gt<Alias>& a, const Gt<Alias>& b)
{
    return op_get(a) > op_get(b);
}

template <class Alias>
struct Leq {};

template <class Alias>
auto operator<=(const Leq<Alias>& a, const Leq<Alias>& b)
{
    return op_get(a) <= op_get(b);
}

template <class Alias>
struct Geq {};

template <class Alias>
auto operator>=(const Geq<Alias>& a, const Geq<Alias>& b)
{
    return op_get(a) >= op_get(b);
}

}

namespace typing {

template <typename Self>
struct Cmp:
    compare::Eq<Self>,
    compare::Neq<Self>,
    compare::Lt<Self>,
    compare::Gt<Self>,
    compare::Leq<Self>,
    compare::Geq<Self>
{};

}

#endif
