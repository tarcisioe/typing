#ifndef TYPING_UTILS_H
#define TYPING_UTILS_H

#include <utility>

namespace typing {

template <typename T, typename Tag>
class Alias;

namespace utils {

namespace detail {

template <class T, typename Tag>
T underlying(Alias<T, Tag>);

}

template <class Alias>
using underlying_type =
    decltype(detail::underlying(std::declval<std::decay_t<Alias>>()));

}

template <template <class> class Op, class Alias>
decltype(auto) op_get(const Op<Alias>& a)
{
    return get(static_cast<const Alias&>(a));
}

}

#endif
